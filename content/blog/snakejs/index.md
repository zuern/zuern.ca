---
title: "SnakeJS - The classic game implemented in JavaScript"
date: 2016-12-08T04:18:26.000Z
---

![Screenshot](Screenshot_20161207_233032.png)

I remember the first mobile phone game I had ever played. Snake was the
classic game where you are a moving dot on a grid, with the objective of
colliding with randomly spawned "food" dots. If you collide with them,
you "ate" them and your tail would grow by a square.
If you collided with the walls of the game area, or with your own tail,
you lose. The objective was to grow the tail as long as possible.

I was thinking about this game recently and it brought back some
nostalgia. I remember really enjoying the game and began to wonder about
how it worked. As I thought about how it might have been written I
realized that I could make this a fun project. I was just learning
JavaScript at the time, and this was a perfect opportunity to practice
my skills, and create a pure HTML, CSS, and JavaScript implementation of
the game.

I decided to fully separate the game logic from the display logic, such
that the game logic could be loaded into another implementation
(possibly a desktop app?) without any modifications to the core logic.
One would simply need to write the code to update whatever UI was being
used with the current game state.

With this design goal in mind, I set out to implement one of my favorite
childhood games.

Although this was a relatively simple project, it provided a great
opportunity to concentrate on writing clean, modular, and flexible
logic. I took my time when writing this project, and I'm really proud
of the result. Not just the game and how it plays, but the code as well.
I think it turned out great.

Here's the project on [GitLab](https://gitlab.com/zuern/snake)
