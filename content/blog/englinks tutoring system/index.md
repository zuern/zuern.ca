---
title: "EngLinks Tutoring System"
date: 2016-12-08T04:05:03.000Z
---

I was volunteering for [Code the
Change](http://queenscodethechange.com/) as a Project Manager for their
annual code jam. I met up with a client and developed a project
specification for them. I then oversaw and led the development of the
product.

My client is part of the Engineering Society of Queen's. They wanted us
to develop a custom tutoring system for their website. Furthermore, they
wanted it to seamlessly integrate with their existing content management
system - WordPress.

After a few days of careful preparation I was ready for the hackathon. I
lead a team of 10 in the development and implementation of this plugin,
working on a remote test server so as not to disrupt the client's
operations. We got quite a bit done however it was not enough in the 10
hours we had to finish the project. I ended up putting in an additional
30-40 hours myself to finish the project.

The Englinks plugin has since been used by thousands of students,
tutors, and administrators, and its userbase is still growing.

> I have been using the plug-in you developed through code the change
> \[sic\] for EngLinks, and it has been instrumental in improving
> EngLinks' services this year. It is very helpful - I can't imagine
> doing my job without it."

-   Taylor Sawadsky (EngLinks Coordinator 2015)

{{% splash %}}
![Screenshot 1](englinks_screenshot--1-.png)
{{% /splash %}}

{{% splash %}}
![Screenshot 2](englinks_screenshot--2-.png)
{{% /splash %}}

## Updates

-   *Nov 2015:* EngLinks contracted me to implement new features and
    maintain the plugin.
-   *Dec 2015:* Plugin was again updated with UI improvements and new
    features
