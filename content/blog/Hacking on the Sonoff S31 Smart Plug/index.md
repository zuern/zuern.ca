---
title: "Hacking on the Sonoff S31 Smart Plug"
date: 2022-02-05T22:09:18-05:00
---

![Sonoff Smart Plug](sonoff_s31.png)

The Sonoff S31 15A WiFi Smart Plug is a smart plug which is well suited to smart home applications, with a bit of effort.

Out of the box it relies on a sketchy app and some random servers on the internet to do command and control. But, under the plastic housing is an ESP 8266 micro-controller. This is a special chip which is easily programmable with open-source tooling and can be programmed with custom C firmware or MicroPython firmware.

The plug can be taken apart and flashed with the open source [Tasmota](https://tasmota.github.io/docs/) firmware to allow it to be controlled locally via HTTP/MQTT APIs or via a server integrated with [HomeAssistant](https://www.home-assistant.io/). This allows fully-local control of the plug without relying on an internet connection and random servers on the internet to continue functioning.

## Flashing the device with Tasmota
The plug can be taken apart easily with a plastic prying tool on the gray end-cap. From there the rails covering the screws are slid off and the screws removed.

Once the plug is taken apart, the micro-controller can be found perpendicular to the main board. It has some debugging pads upon which wires can be soldered.

![](1114.jpg)

In the picture above, I (poorly) soldered to the VCC (3.3 volt) pad, RX (receive), TX (transmit), and GND (Ground).

Once soldered on, I connected the wires to a USB to serial adapter which allowed me to connect to and power the micro-controller from my computer.
I was using a CP2102 USB to TTL Serial Adapter (pictured below) with the following mapping to connect the wires from the adapter to the smart plug:
- 3v3 to VCC
- TXD to RX
- RXD to TX
- GND to GND.
- Do **NOT** connect the 5V on the adapter to the smart plug. ESP 8266 use 3.3V and it'll fry the board


![cp2102 usb to ttl serial adapter](cp2102%20usb%20to%20ttl%20serial%20adapter.png)

![](21-12-31%2016-29-01%201113.jpg)

Now with everything connected I started to flash the firmware. For this I used [Tasmotizer](https://github.com/tasmota/tasmotizer) with the following steps:
```bash
git clone https://github.com/tasmota/tasmotizer
cd tasmotizer
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
sudo python3 tasmotizer  # Had to run as root because serial port required elevated privileges to write to.
```

Once open, I followed the Tasmota docs to download a `tasmota.bin` firmware image and use Tasmotizer to flash it to the device.

## Configuring Tasmota
Once the firmware was flashed I power-cycled the device while still connected to the computer for power. Using my phone I joined the newly appearing "Tasmota ..." Wi-Fi network created by the smart plug. My browser opened automatically and I selected the Wi-Fi network SSID and password for the smart plug to connect to.

Once set up, the plug runs a web server which can be connected to to remotely toggle the switch.

![](Pasted%20image%2020220102024521.png)

## Future Work
I use [NATS](https://nats.io) for my MQTT broker and intend to control this smart plug and other devices over MQTT. However my MQTT setup is protected with TLS, which may require custom firmware on the device to provide the certificate fingerprint. Alternatively I could _ignore_ the server certificate fingerprint and trust it anyways however I need to think about the kind of threat model that would entail.
