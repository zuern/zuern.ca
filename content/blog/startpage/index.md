---
title: "A custom homepage with AJAX comics & weather"
date: 2016-12-24T05:43:36.000Z
---

This mini-project was primarily to save myself time. I have wanted a
custom "dashboard" for my homepage in my browser for quite some time.
Today was the day that I created it. The main features are:

1.  [DuckDuckGo](https://duckduckgo.com) Embedded Search Bar
2.  AJAX loaded XKCD Comic of the Day
3.  AJAX loaded, custom-designed weather forecasts
4.  Quick Links to my frequently visited websites

I built the project as a NodeJS Express App which fetches weather data
from the [DarkSky API](https://darksky.net) and the latest [XKCD
Comic](http://xkcd.com) and passes them to the front-end in a single
request.

I used the backend to fetch the data so I could implement `node-cache`
(similar to memcached) to reduce the number of API requests to DarkSky.
It also allowed me to create a `config.js` file which contains a simple
JSON configuration with API keys, coordinates for weather forecasts, and
link information to generate the navbar.

The homepage is live at <https://start.zuern.ca>, and code
available on [GitLab](https://gitlab.com/zuern/startpage)

![Screenshot 1 of app](startkevin1.png)

![Screenshot 2 of app](startkevin2.png)
