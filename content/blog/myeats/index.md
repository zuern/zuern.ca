---
title: "myEats: a grocery inventory/recipe search tool"
date: 2017-02-13T20:55:35.000Z
---

This project was created during the [QHacks 2017](http://qhacks.io)
hackathon. myEats is designed to make it easier to track which food you
have at home and to find new recipes to cook with the things you have on
hand.

![Screenshot of myEats app](myEats_002.png)

It keeps a list of the items you have on hand, with sliders for each
item to keep track of how much you have left. When you finish an item
you can delete it from the list. We also built in a (beautiful) recipe
search that lets users search a database of over 1.5 million recipes via
the [Edamam API](https://developer.edamam.com/).

I built this together with my friends
[Deven](https://github.com/devenbernard/),
[Marissa](https://github.com/marissahuang/), and
[Emma](https://github.com/efletch13). The app was built with NodeJS,
Express, Knockout and a sprinkling of jQuery. The persistence layer was
CouchDB.

We started this project by planning for several hours what features we
would like to see in the app - as well as how we would like users to
interact with it. Once we had settled on our initial concept we fleshed
out some features on a kanban board, assigned tasks, and went off. The
hackathon itself was designed to be an all-night type hackathon.

The first night of coding finished and we had fleshed out the individual
components of the application. I had set up and configured a database
service layer for the application. Deven, Marissa, and Emma had each
taken on their own roles in the development cycle with Deven handling
the front end viewmodels and knockout templates, Marissa managing the
CSS and creating a great flexbox layout, and Emma designing and creating
the user interface.

The second night was spent further improving and polishing up the user
interface. We stopped development midway through the night to put our
current build up on the projector and run through a "demo" of the
features. During the demo we noticed many things that needed to be fixed
- colours on the page being too similar causing confusion, bugs, missing
features, etc.

After reviewing the list of improvements to make we got back to work.
After 24+ hours of continuous programming we managed to finish the
application minutes before the deadline. The code for this project is available on
[GitLab](https://gitlab.com/zuern/qhacks2017/)

![](myEats_001.png)

Next Steps
----------

We were planning to implement an OCR type scanner into the app to allow
users to "scan" their grocery receipts and automatically input all of
the purchased items into the app. We would have liked to implement this
feature, but didn't end up having enough time. Perhaps this can be
added after the fact on a weekend some day.
