---
title: "Queen's Housing Helper"
date: 2016-12-08T05:27:12.000Z
---

![Screenshot](Screenshot_20161208_002832.png)

Queen's was hosting its inaugural "Local Hack Day" which was
sponsored by [Major League Hacking](http://mlh.io), Queen's Software
Developer's Club, and [QHacks](http://qhacks.io). This was a one day
hackathon with a central theme around creating software to help Queen's
and/or the greater Kingston community.

I was in a group with my lovely friends
[Marissa](http://github.com/marissahuang),
[Emma](https://github.com/efletch13), and
[Brendan](https://github.com/bKolisnik). They were all more or less
completely new to HTML and CSS, and only Brendan had worked with
JavaScript before. We decided to use the hackathon as a learning
opportunity and just have fun.

Our idea (courtesy of Marissa) was to build a simple website to help
students find a place to rent, with helpful filters such as:

-   Distance to Campus
-   Cost
-   Utilities included

and so on. The standard repertoire of filters was to be included. What
set our site apart from the rest is that it would scrape classified ad
places, and provide a large map view with pins for each of the available
units. Clicking on a pin would pull up a detail view of the unit.

The app was built using the MEAN stack, along with the standard front
end libraries (jQuery, Knockout, FontAwesome, Bootstrap...). I did my
best to explain HTML and CSS development as we went, eventually handing
off the responsibility of developing the front end to Marissa and Emma.
Brendan and I got started implementing the backend. I worked on the API
and server while Brendan wrote a service to interface with MongoDB.

By the end of the hackathon (which was only 12 hours) we had a (mostly)
working demo of the app, with sample data pulled from Kijiji and other
sources. The proof of concept was definitely there. We all had a blast
developing it, and I will continue to work on it here and there when I
have time. I really think this could help students find their ideal
place to stay.

As always, the code is available on
[GitLab](https://gitlab.com/zuern/lhd-housinghelper).

