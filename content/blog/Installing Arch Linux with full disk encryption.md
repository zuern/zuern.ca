---
title: "Installing Arch Linux with full disk encryption"
date: 2019-05-15T00:11:41-04:00
---

<!--
TODO:
- Bigger display size for GRUB
- Install script (maybe?)
- Terminus Fonts
- fwupd?
-->

For this guide you will be installing Arch and booting from a UEFI system using
GPT partitioning setup. In plain English: Full disk encryption on your laptop so
you can be sure that if your device is stolen, your files and data will be safe
and sound.

**Note**: This configuration leaves the boot partition unencrypted. This means
that a malicious actor could replace your boot loader with anything and could
compromise your decryption passphrase if you boot using malicious software. So
if your threat model includes people with physical access to tamper with your
computer, then you should either encrypt the boot partition as well, or store
the boot partition on a USB that you always carry with you so that you can be
sure it hasn't been messed with.

Features:
- Full disk encryption (except boot partition)
- Partition scheme that allows you to replace/reinstall the OS without touching
	your personal files or settings
- High DPI font support in terminal

## Creating the Install Media
Head over to the Arch [download page](https://www.archlinux.org/download/) and
fetch yourself a copy of the ISO. I recommend using a BitTorrent download so you
will get the download quicker and from the community rather than the Arch
servers (let's save their bandwidth when we can).

Then install [Balena Etcher](https://www.balena.io/etcher/), a great tool to
create live-USBs.

Using Etcher, create a bootable USB with the arch ISO image you downloaded.

## Installing Arch Linux
First step is to boot the live-USB. That varies from system to system so I leave
it up to you to figure it out.

**Important:** Make sure to boot in UEFI mode (Usually you'll be given a choice
between UEFI and Legacy Mode).

When I first used the live USB I thought I'd need a microscope to finish
installing since the font was so small! Luckily there is a pretty easy fix.
Simply set the [console
font](https://wiki.archlinux.org/index.php/Console_fonts) to a bigger one with
`setfont latarcyrheb-sun32`.

You will need to follow the [installation guide](https://wiki.archlinux.org/index.php/Installation_guide) to install Arch.
Please read everything in the colored boxes (both tips and notes) as there is
very important information in there -- which if you do not read -- may prevent
you from successfully installing Arch.

**I will only list out the things you need to do differently (in order) here
so follow my instructions instead of the wiki for any sections in the guide
below.**

## Setting up the base system

### Create and format disk partitions
Figure out the name of the disk you want to install on using `lsblk`. Usually
for hard drives it will be something like `/dev/sda` or `/dev/nvme0n1` for NVME
SSDs common in laptops. Pay special attention to the drive capacity as you want
to make sure you install to the correct drive!

Once you've identified the disk to install onto use the interactive partitioning
tool `cfdisk` to create the following partitioning scheme (assuming `sdX` is the
drive name you want to install to):

| Partition    | Partition type        | Size                               |
| :----------- | :-------------------- | :--------------------------------- |
| `/dev/sdX1`  | EFI System Partition  | 512                                |
| `/dev/sdX2`  | Linux File System     | 100%FREE (Use the remaining space) |


Now it's time to create filesystems on the partitions. First the filesystem for
the EFI boot partition: `mkfs.fat -F32 /dev/sdX1`. We are using FAT32 for `sdX1`
because EFI requires the format to be FAT32.

Next we're going to set up our encrypted partition and LVM on top
([LVM on LUKS](https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS)).
The rationale here is to use LUKS as a base so all LVM partitions above are
encrypted, and those LVM partitions are resizeable. If you ever need to shrink
your home partition to make space for more programs on the root, or otherwise
resize partitions you will be able to do it.

```bash
# Create the encrypted partition
cryptsetup luksFormat /dev/sdX2
# Open the encrypted partition
cryptsetup open /dev/sdX2 cryptlvm
# Create the LVM Physical Volume
pvcreate /dev/mapper/cryptlvm
# Create the LVM Volume Group
vgcreate ArchVolGroup /dev/mapper/cryptlvm
# Create LVM Logical Volumes for the different partitions
lvcreate -L 50G ArchVolGroup -n root
lvcreate -l 100%FREE ArchVolGroup -n home
# Create the file systems on those logical volumes
mkfs.ext4 /dev/ArchVolGroup/root
mkfs.ext4 /dev/ArchVolGroup/home
```

### Install base packages for Arch
It's now time to run `pacstrap` which will set up our base installation of Arch
Linux. But first we need to mount the filesystems where Arch should be
installed and create some basic directories:

```bash
mount /dev/ArchVolGroup/root /mnt
mkdir /mnt/home
mount /dev/ArchVolGroup/home /mnt/home
mkdir /mnt/boot
mount /dev/sdX1 /mnt/boot
```

Now that everything is mounted we can run `pacstrap` to bootstrap our
installation.
```bash
pacstrap /mnt base base-devel linux linux-firmware vim terminus-font lvm2
```

This will install the base packages and a few extras:
- `nvim` - My text editor of choice.
- `lvm2` - (needed for LVM + LUKS encryption)
- `terminus-font` - A much more readable console font as well.


---
**After this point I assume you have Chrooted into the new system via:** `arch-chroot /mnt`

---

Make sure you are chroot-ed into the new system, so that these commands
following will apply to the new Arch installation.

## Setting up boot configuration

Now that the base packages are in place we can set up our boot configuration.
The first thing to do is configure the initramfs to decrypt the LUKS partition
on boot. Edit the "HOOKS" section in `/etc/mkinitcpio.conf` to look like below.
When done, run `mkinitcpio -p linux`

```bash
# /etc/mkinitcpio.conf:

HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 filesystems keyboard fsck)
```

Next set up a swapfile. This is nicer than a swap partition because it can be
resized to fit your needs. I recommend to make it half the size of your RAM.
Follow the [Arch Wiki: Swap
Files](https://wiki.archlinux.org/index.php/Swapfile#Swap_file) page to set it
up.

Don't forget to install the microcode package for your system! For Intel:
```bash
pacman -S intel-ucode
```

Install the grub boot loader:
```bash
pacman -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
```

Edit GRUB default config `/etc/default/grub`

- Add the following to the `GRUB_CMDLINE_LINUX`: `cryptdevice=/dev/sdX2:cryptlvm root=/dev/ArchVolGroup/root`
- Add the following to the `GRUB_PRELOAD_MODULES` (might not be necessary but not sure): `lvm`

The changed lines should look something like the following:
```bash
GRUB_CMDLINE_LINUX="cryptdevice=/dev/sdX2:cryptlvm root=/dev/ArchVolGroup/root"
GRUB_PRELOAD_MODULES="part_gpt part_msdos lvm"
```
Generate the grub config:
```bash
grub-mkconfig -o /boot/grub/grub.cfg
```

### Tip: Better console font on boot
Install `terminus-font` and enable as the default console font on boot. It's
much nicer than what comes out of the box. To do so run
`pacman -S terminus-font` and then update `/etc/vconsole.conf` to set the font:

```bash
# /etc/vconsole.conf:

FONT=ter-132n
```

---

Follow any additional steps to set up your system as you wish. Once done, exit
the chroot environment, unmount your partitions and reboot into your new OS.
From here you can install your additional packages as you please. Welcome to
Arch!
