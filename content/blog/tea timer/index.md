---
title: "Tea Timer"
date: 2016-12-08T06:04:10.000Z
---

This was a **very** simple web app I wrote as part of a workshop on
JavaScript app development. Attendees cloned the repository and
developed the app on their own machines as we progressed through the
workshop. At the end of the hour, everyone had a working product. It
gave a great intro to JavaScript and was a very successful workshop. I
decided to write about it because I really like the design and
simplicity of it.

The code is available on [GitLab](https://gitlab.com/zuern/tea-timer)

![Screenshot](Screenshot_20161208_010358-1.png)
