---
title: "Improving pagerank and building your audience"
date: 2017-02-22T20:31:57.000Z
draft: true
Description: "Google's business is built on providing the most relevant, best search results for its users. So, if you want to rank well, you should be the most relevant, and best page for the things your customers search for."
---

Google's business is built on providing the most relevant, best
search results for its users. So, if you want to rank well, you should
be the most relevant, and best page for the things your customers search
for.

This boils down to a few main objectives:

1.  Make your website excellent from a technical standpoint

	-   Make your site secure. Run your website only over https. Use
			services like [Let's Encrypt](https://letsencrypt.org) to get a
			free SSL certificate.
	-   Make your page fast. Use tools like the [Chrome Developer
			tools](https://developers.google.com/web/tools/chrome-devtools/) to
			audit and improve your website's performance.
	-   Make your site accessible. I cannot stress this enough.
			Differently abled users of the web will find this immensely
			valuable. I'm sure Google will give you brownie points for this as
			well. Use [automated checkers](http://achecker.ca/checker/index.php)
			to see how your site fares.

2.  Make your content excellent
3.  Build authority on the web

	-   Get sites to link to your content.
	-   Get people talking about your site on social media
	-   Make your website a source of information for your target audience
	-   Try to build a community around your business/product.

4.  Marketing, Marketing, Marketing!

	-   Use advertising campaigns
	-   Use social media to advertise
	-   Get your customers talking about you
	-   Set up a referral program

5.  Collect data and revise your strategy

	-   A data-driven approach is the key to success here.
	-   Be objective and back up your decisions with empirical data.


## Content improvements

Remember, there is no formula to improving your pagerank. Google and
friends do NOT tell webmasters how they rank pages so it remains fair to
the users. If you focus your efforts on improving your page and
your content you will rise up the ranks. If you focus on building an
audience that talks about your page and links to your page -
especially on social media, that will help very much. Taking steps to
make your website as easy to use as possible (and accessible for people
with disabilities) will help your page rank because it helps the users
of your website.

Search engines don't rank pages for the businesses, they rank them to be
most useful for what the users are looking for. So make sure that
what you have to say on your website is exactly what your desired
users are looking for. You can improve your chances by keeping a blog
with articles/tutorials for your potential/current customers that talks
about things they would find useful.

## Cross linking, authority, and building user-base

You want to be the experts that people come to for information. You
guys are the pros - that's why people should trust you to build their
website. Don't just try to sell your stuff. Try to really help your
target audience. Articles and tutorials will go a long way. Once you
have these written, go to help forums and search for folks who have
questions you've answered in your posts. Link them to your website to
build up authority for your website. The more people/places that are
pointing to your site, the more important/relevant it must be right?

You want your website to become a valuable resource - not just for
your customers, but for anyone in your target demographic - folks who
want a website but probably don't know where to start or what to do. By
becoming a good source of information you will be helping your existing
customers, and attract new ones to your site.

This positions you to be the experts and will make people come to you
for information. They will trust what you have to say, and they will
trust that you know what you are doing. This helps you convince them
your services are exactly what they needed.

## Referrals, social media, and advertising

This section is the "secret sauce" that many businesses miss out on.
Here is where you can really drive traffic and sales to your website.
Doing this right can make a massive impact on your business. The basic
premise is to use your customers' social networks to find new customers.

### Referrals

The best advertising is the advertising your users do for you!

> The most credible form of advertising comes straight from the people
> we know and trust. Eighty-three percent of online respondents in 60
> countries say they trust the recommendations of friends and family,
> according to the Nielsen Global Trust in Advertising Report - [Nielson
> 2015](http://www.nielsen.com/us/en/press-room/2015/recommendations-from-friends-remain-most-credible-form-of-advertising.html)

Set up a referral program for your customers. Make them receive a free
month of hosting or something for every new customer they refer to you
that signs up. Give both parties involved a reward! This will make
the existing customer feel like they are "doing them a favor" and the
incentivise them to refer more people. By giving both the referrer and
referee a reward you will make it benefit both people, and most
importantly, your company because you will have a new customer! Uber
does this very well. Look at [this case
study](https://www.referralsaasquatch.com/how-uber-taught-me-everything-about-referrals/).

Referral programs only work if the reward is worth the effort for the
customer.

This means there are two ways to make the referral program better:

1.  Make the reward better
2.  Make the referral process easier

You can calculate how much to make the reward based on the future
revenue the new customer will bring in. If each new customer brings in
an average of $500 on sign up and $2000 over 1 year then you could
easily justify giving $100 off to the new customer and a $50 credit to
the existing customer who referred them.

The referral process should feel natural and very easy. If your
customers have a log in or dashboard, put a referral link right on that
so every time they log in they see it. They should know about the
referral program without needing to search for it or be told by you. The
referral process should be as simple as filling out their friend's email
address and their friend clicking the link to sign up. Make it as easy
as possible.


## Social media & advertising


The fastest ways to bring traffic to your website are through social
media. Research which social media your target audience use the most and
make sure you have a presence there. Then go on those platforms and
advertise your website. Follow the [guidelines for constructing good
advertisements](https://support.google.com/adwords/answer/1704392?hl=en).
Use specific and targeted keywords in your ads.

### Don't do this:

<div style="border: 1px solid #000; display: inline-block; max-width:500px; padding: 0 15px 0 0;">
<b>Adam's Apples</b>
<br>
<i>Great apples at great prices!</i>
<br>
<a href="#">http://adamsapples.com</a>
</div>

### Do this instead:

<div style="border: 1px solid #000; display: inline-block; max-width:500px; padding: 0 15px 0 0;">
<b>Adam's Apples</b>
<br>
<i>Enjoy a farm-fresh Ontario apple as part of your healthy breakfast!</i>
<br>
<a href="#">http://adamsapples.com</a>
</div>


Try to write ads that target a specific need. Don't write generic
one-size-fits-all advertisements. Create ads that make the user want to
learn more. You can even advertise your helpful articles and guides.
Create multiple specific ads and run them simultaneously. Experiment
with different wording and target audiences. See what works for you.

Pair your Adwords and Social Advertising Accounts to your Analytics
Account

This will allow you to track users as they move from social media to
your website and see where they go on your page. This information will
help you create
[funnels](https://blog.crazyegg.com/2014/03/03/website-conversion-funnel/)
that channel your audience and push them to a purchase. [Use Analytics
to set up and track the success of your
funnels](https://blog.kissmetrics.com/conversion-funnel-survival-guide/).

## Wrapping up

This is by no means a comprehensive guide on increasing traffic to your
website and improving pagerank. There is so much more that can be done.
The biggest takeaway from this is that you should not focus on
improving your pagerank and traffic, but instead should focus on
attracting the audience you want to convert to customers. Do this by
making your website secure, easy to use, and accessible. Do this by
providing useful and relevant content and building reputation as a good
source of information. Do this by building up links to your webpage. Do
this by leveraging your customers' social networks to find new prospects
and funnel them through your site.

Analytics are everything here. Use data to drive your decisions and
drive your sales.
