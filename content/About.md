---
title: "About"
date: 2016-12-08T06:27:30.000Z
---

I'm an energetic and light-hearted developer. I am a daily Linux user, and
am passionate about technology and especially open-source software. My main
interests lie in backend software development and building small tools to make
my day-to-day life easier. I also enjoy the challenge of reducing my reliance on
cloud services and hosting my own services on my own network.

Outside of software I enjoy biking, canoeing, hiking, and being outdoors.
