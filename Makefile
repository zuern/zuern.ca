DOCKER_REGISTRY=registry.gitlab.com/zuern/zuern.ca
DOCKER_IMAGE=zuern.ca
VERSION=$(shell git describe --tags --always --dirty)

# Verbose output
V=0
Q = $(if $(filter 1,$V),,@)

all:
	$Q hugo

dev:
	$Qhugo server --port 3000 --buildDrafts --disableFastRender

prod: clean
	$(info Building site)
	$Qhugo --environment production --enableGitInfo
	$(info Building docker image)
	$Qdocker build --platform linux/amd64 . -t $(DOCKER_IMAGE):latest

release: prod
	$(info Tagging image $(DOCKER_REGISTRY)/$(DOCKER_IMAGE):$(VERSION))
	$Qdocker tag $(DOCKER_IMAGE):latest $(DOCKER_REGISTRY)/$(DOCKER_IMAGE):$(VERSION)
	$Qdocker tag $(DOCKER_IMAGE):latest $(DOCKER_REGISTRY)/$(DOCKER_IMAGE):latest
	$(info Pushing image to registry)
	$Qdocker push $(DOCKER_REGISTRY)/$(DOCKER_IMAGE):latest
	$Qdocker push $(DOCKER_REGISTRY)/$(DOCKER_IMAGE):$(VERSION)

clean:
	$Qrm -rf public

.PHONY: dev prod release clean
